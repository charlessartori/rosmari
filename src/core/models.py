#coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _

########################################################################################
######### Model Contact ################################################################

class KindContactManager(models.Manager):
    def __init__(self, kind):
        super(KindContactManager, self).__init__()
        self.kind = kind

    def get_query_set(self):
        qs = super(KindContactManager, self).get_query_set()
        qs = qs.filter(kind=self.kind)
        return qs

class Contact(models.Model):
    KINDS = (
        ('P',_(u'Telefone')),
        ('E',_(u'E-mail')),
        ('F',_(u'Facebook')),
        ('S',_(u'Skype')),
        ('T',_(u'Twitter')),
        ('M',_(u'MSN')),
        ('A',_(u'Endereço')),
        ('O',_(u'Outros')),
    )

    kind = models.CharField(_(u'Tipo'), max_length=1,choices=KINDS)
    value = models.CharField(_(u'Valor'), max_length=255, null=True, blank=True)
    url = models.URLField(_(u'Url'),null=True, blank=True)
    created_at = models.DateTimeField(_(u'Criado em'), auto_now_add=True)

    objects = models.Manager()

    class Meta:
        verbose_name = u"Contato"
        verbose_name_plural = u"Contatos"

########################################################################################
######### Model Message ################################################################

class Message(models.Model):
    KINDS = (
        ('D',_(u'Dúvida')),
        ('S',_(u'Sugestão')),
        ('C',_(u'Crítica')),
        ('O',_(u'Outro')),
    )
    name = models.CharField(_(u'Nome'), max_length=255)
    email = models.EmailField(_(u'E-mail'))
    kind = models.CharField(_(u'Assunto'), max_length=1,choices=KINDS)
    message = models.TextField(_(u'Mensagem'))
    feed = models.BooleanField(_(u'Receber e-mails'),default=True)
    created_at = models.DateTimeField(_(u'Recebido em'), auto_now_add=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["created_at"]
        verbose_name = u"Mensagem"
        verbose_name_plural = u"Mensagens"

########################################################################################
######### Model About ################################################################

class About(models.Model):
    name = models.CharField(_(u'Nome do Projeto'), max_length=255)
    about = models.TextField(_(u'Sobre'))
    created_at = models.DateTimeField(_(u'Criado em'), auto_now_add=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"Sobre"
        verbose_name_plural = u"Sobre"


