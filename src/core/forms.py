#coding: utf-8
__author__ = 'charles'

from django import forms
from .models import Message
from django.utils.translation import ugettext as _

class MessageForm(forms.ModelForm):

    def __init__(self,*args,**kwargs):
        super(MessageForm, self).__init__(*args,**kwargs)

        self.fields['name'].required = True

        self.fields['email'].required = True
        self.fields['email'].help_text = _(u"Seu e-mail não será divulgado.")

        self.fields['kind'].required = True
        self.fields['kind'].choices = Message.KINDS

        self.fields['feed'].required = False
        self.fields['feed'].initial = True
        self.fields['feed'].help_text = _(u'Desejo receber novas novidades por e-mail!')

    class Meta:
        model = Message

