#coding: utf-8
from django.test import TestCase
from django.core.urlresolvers import reverse
from .models import Contact,Message,About

########################################################################################
######### Tests Homepage ################################################################

class HomepageView(TestCase):
    def setUp(self):
        self.response = self.client.get(reverse('homepage'))

    def test_get_index_page(self):
        "Verifica rota index.html"
        self.assertTemplateUsed(self.response, 'index.html')

    def test_get(self):
        self.assertEqual(200,self.response.status_code)

    def test_about_in_context(self):
        self.assertIn('about', self.response.context)

    def test_contact_in_context(self):
        self.assertIn('contacts',self.response.context)

    def test_form_in_context(self):
        self.assertIn('form', self.response.context)

    def test_news_in_context(self):
        self.assertIn('news',self.response.context)

    def test_albuns_in_context(self):
        self.assertIn('albuns',self.response.context)


########################################################################################
######### Tests About ################################################################

class AboutTest(TestCase):
    def setUp(self):
        self.about = About(
            name = "rosmari",
            about = "moda rosmari - Santo Ângelo/RS"
        )
        self.about.save()

    def test_create(self):
        self.assertEqual(1,self.about.pk)

    def test_unicode(self):
        self.assertEqual(u'rosmari', unicode(self.about))

########################################################################################
######### Tests Message ################################################################

class MessageTest(TestCase):
    def setUp(self):
        self.message = Message(
            name = 'rosmari',
            email = 'charles.sartori@gmail.com',
            kind = 'D',
            message = 'Ola Charles',
            feed = True
        )
        self.message.save()

    def test_create(self):
        self.assertEqual(1,self.message.pk)

    def test_kinds(self):
        self.assertEqual(Message.KINDS,(
            ('D',(u'Dúvida')),
            ('S',(u'Sugestão')),
            ('C',(u'Crítica')),
            ('O',(u'Outro')),
            ))

    def test_unicode(self):
        self.assertEqual(u'rosmari', unicode(self.message))

########################################################################################
######### Tests Contact ################################################################

class ContactTest(TestCase):
    def test_create_email(self):
        contact = Contact.objects.create(
            kind='E',
            value ='charles.sartori@gmail.com',
            url = 'www.lerolero.com'
        )
        self.assertEqual(1,contact.pk)


    def test_kinds(self):
        self.assertEqual(Contact.KINDS,(
            ('P',(u'Telefone')),
            ('E',(u'E-mail')),
            ('F',(u'Facebook')),
            ('S',(u'Skype')),
            ('T',(u'Twitter')),
            ('M',(u'MSN')),
            ('A',(u'Endereço')),
            ('O',(u'Outros')),
        ))