#coding: utf-8
__author__ = 'charles'


from django.contrib import admin
from .models import Message,Contact,About
from django.utils.datetime_safe import datetime


class MessageAdmin(admin.ModelAdmin):
    list_display = ('name','email','kind','message','feed','created_at','subscribed_today')
    date_hierarchy = 'created_at'
    list_filter = ['kind','created_at']

    def subscribed_today(self, obj):
        return obj.created_at.date() == datetime.today().date()
    subscribed_today.short_description = u'Adicionado hoje?'
    subscribed_today.boolean = True

    def feed(self,obj):
        return obj.feed
    feed.boolean = True

class ContactAdmin(admin.ModelAdmin):
    list_display = ('kind','value','url')

class AboutAdmin(admin.ModelAdmin):
    list_display = ('name','about')

    class Media:
        js = ('/static/js/tiny_mce/tiny_mce.js', '/static/js/tiny_mce/textareas.js',)

admin.site.register(About,AboutAdmin)
admin.site.register(Message,MessageAdmin)
admin.site.register(Contact,ContactAdmin)