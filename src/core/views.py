# Create your views here.
from django.views.generic.simple import direct_to_template
from .models import About,Contact
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from .forms import MessageForm
from src.news.models import News
from src.gallery.models import Album

def homepage(request):

    form = MessageForm(request.POST or None)

    ###### CONTEXT ###################
    about = About.objects.all()
    contacts = Contact.objects.all()
    news = News.objects.all()
    albuns = Album.objects.all()

    context = {'about':about,
               'contacts':contacts,
               'form':form,
               'news':news,
               'albuns':albuns}
    ##################################

    if not form.is_valid():
        return direct_to_template(request, 'index.html',
            context)

    message = form.save()
    return HttpResponseRedirect(reverse('homepage'))
