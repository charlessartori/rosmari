#coding: utf:8
__author__ = 'charles'

from django import forms
from .models import Photo

class FormPhoto(forms.ModelForm):
    class Meta:
        model = Photo
        exclude = ['thumbnail']