#coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _

class Album(models.Model):
    title = models.CharField(_(u"Nome"), max_length=128)
    slug = models.SlugField(unique=True)
    description = models.TextField(_(u"Descrição"))
    created_at = models.DateTimeField(_(u"Criado em"), auto_now_add=True)
    modified_at = models.DateTimeField(_(u"Modificado em"), auto_now=True)

    @property
    def photos(self):
        return self.photo_set.order_by('created_at').reverse().filter(is_cover_photo=False)

    @property
    def cover(self):
        cover = self.photo_set.filter(is_cover_photo=True)
        return cover.get().original.url

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ('-created_at',)
        verbose_name = u"Álbum"
        verbose_name_plural = u"Álbuns"

class Photo(models.Model):
    title = models.CharField(_(u"Título"), max_length=256)
    description = models.TextField(_(u"Descrição"), blank=True, null=True)
    created_at = models.DateTimeField(_(u"Criado em"), auto_now_add=True)
    modified_at = models.DateTimeField(_(u"Modificado em"), auto_now=True)

    original = models.ImageField(_(u"Imagem"), null=True, blank=True, upload_to='galeria/fotos')
    thumbnail = models.ImageField(_(u"Imagem Thumbnail"), null=True, blank=True, upload_to='galeria/thumbnail')

    album = models.ForeignKey(Album)
    is_cover_photo = models.BooleanField(_(u"É capa do Álbum?"))


    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ('-created_at','album')
        verbose_name = u"Foto"
        verbose_name_plural = u"Fotos"


    def save(self):
        if self.is_cover_photo:
            other_cover_photo = Photo.objects.filter(album=self.album).filter(is_cover_photo = True)
            for photo in other_cover_photo:
                photo.is_cover_photo = False
                photo.save()
        super(Photo, self).save()


    def get_cover_photo(self):
        if self.photo_set.filter(is_cover_photo=True).count() > 0:
            return self.photo_set.filter(is_cover_photo=True)[0]
        elif self.photo_set.all().count() > 0:
            return self.photo_set.all()[0]
        else:
            return None
