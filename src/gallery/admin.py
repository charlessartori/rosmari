#coding: utf-8

__author__ = 'charles'

try:
    import Image
except ImportError:
    from PIL import Image

from django.contrib import admin
from .models import Album,Photo
from .forms import FormPhoto

class AlbumAdmin(admin.ModelAdmin):
    list_display = ('title','description',)
    search_fields = ('title',)
    prepopulated_fields = {'slug': ('title',)}

class PhotoAdmin(admin.ModelAdmin):
    list_display = ('title','description','is_cover_photo','album','thumbnail_html')
    list_filter = ('album',)
    search_fields = ('title','description',)
    form = FormPhoto


    def thumbnail_html(self,obj):
        return u'<img src="%s" alt="Imagem Thumbnail"/>' % (obj.thumbnail.url)
    thumbnail_html.allow_tags = True

    def save_model(self, request, obj, form, change):
        """Metodo declarado para criar miniatura da imagem depois de salvar"""
        super(PhotoAdmin, self).save_model(request, obj, form, change)

        if 'original' in form.changed_data:
            extensao = obj.original.name.split('/')
            obj.thumbnail = 'galeria/thumbnail/%s'%(extensao[-1])

            mini = Image.open(obj.original.path)
            mini.thumbnail((260,180), Image.ANTIALIAS)
            mini.save(obj.thumbnail.path)

            photo = Image.open(obj.original.path)
            photo.thumbnail((500,500), Image.ANTIALIAS)
            photo.save(obj.original.path)

            obj.save()

admin.site.register(Album,AlbumAdmin)
admin.site.register(Photo,PhotoAdmin)