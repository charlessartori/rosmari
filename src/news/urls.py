#coding: utf-8

__author__ = 'charles'

from django.conf.urls import patterns,include,url

urlpatterns = patterns('src.news.views',
    url(r'^(?P<slug>[\w-]+)/$', 'news_detail',
        name='news_detail'),

)