#coding: utf-8

from django.test import TestCase
from django.core.urlresolvers import reverse
from .models import News,Media,Comments

########################################################################################
######### Tests Index ################################################################


class IndexViewTest(TestCase):
    def setUp(self):
        self.response = self.client.get(reverse('homepage'))
        self.news = News.objects.create(
            title = u'Django é D+',
            content = u'E tenho dito!'
        )
    def test_get_index_page(self):
        "Verifica rota index.html"
        self.assertTemplateUsed(self.response, 'index.html')

    def test_about_in_context(self):
        self.assertIn('about', self.response.context)

    def test_contact_in_context(self):
        self.assertIn('contacts',self.response.context)

    def test_form_in_context(self):
        self.assertIn('form', self.response.context)

    def test_news_in_context(self):
        self.assertIn('news',self.response.context)

    def test_albuns_in_context(self):
        self.assertIn('albuns',self.response.context)



########################################################################################
######### Tests News ################################################################


class TestModelNews(TestCase):
    def test_model_news(self):
        s = News.objects.create(
            title = 'Django é D+',
            content = 'E tenho dito!'
        )
        self.assertEquals(s.id,1)

class NewsDetailView(TestCase):
    def setUp(self):
        News.objects.create(
            title='Django legal',
            slug='django-legal',
            content='lero-lero'
        )
        self.response = self.client.get(reverse('news:news_detail',
            kwargs={'slug': 'django-legal'}))

    def test_get(self):
        self.assertEqual(200,self.response.status_code)

    def test_template(self):
        self.assertTemplateUsed(self.response,'news/news_detail.html')

    def test_news_in_context(self):
        self.assertIn('news',self.response.context)

    def test_form_in_context(self):
        self.assertIn('form',self.response.context)

    def test_about_in_context(self):
        self.assertIn('about',self.response.context)

    def test_contacts_in_context(self):
        self.assertIn('contacts',self.response.context)




########################################################################################
######### Test Media ####################################################################

class MediaModelTest(TestCase):
    def setUp(self):
        news = News.objects.create(
            title='Django',
            slug='django-legal',
            content='lero-lero'
        )
        self.media = Media.objects.create(
            news = news,
            type='YT',
            media_id='QjA5faZF1A8',
            title='Video'
        )

    def test_create(self):
        self.assertEqual(1,self.media.pk)

    def test_unicode(self):
        self.assertEqual("Django - Video", unicode(self.media))





