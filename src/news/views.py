#coding: utf-8

from django.shortcuts import get_object_or_404
from django.views.generic.simple import direct_to_template
from .models import News
from src.core.models import About, Contact
from src.core.forms import MessageForm
from src.gallery.models import Album

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse



def news_detail(request, slug):

    form = MessageForm(request.POST or None)

    ###### CONTEXT ###################
    about = About.objects.all()
    contacts = Contact.objects.all()
    albuns = Album.objects.all()
    news = get_object_or_404(News,slug=slug)


    context = {'about':about,
               'contacts':contacts,
               'form':form,
               'news':news,
               'albuns': albuns}
    ##################################


    return direct_to_template(request, 'news/news_detail.html',
                            context)

