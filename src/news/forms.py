#coding: utf-8
__author__ = 'charles'

from django import forms
from .models import Comments,News

class CommentForm(forms.ModelForm):
    name = forms.CharField(required=True)
    news = forms.ModelChoiceField(queryset=News.objects.all(), widget=forms.HiddenInput, label='')
    class Meta:
        model = Comments
